import { View, Text } from 'react-native'
import React from 'react'
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native'
import Login from './Login';
import Home from './Home';
import AddData from './AddData';
// import coba from './coba';

const Stack = createNativeStackNavigator();
const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='Login' component={Login}/>
        <Stack.Screen options={{headerShown:false}} name='Home' component={Home}/>
        <Stack.Screen options={{headerShown:false}} name='AddData' component={AddData}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default Routing