import {combineReducers} from 'redux'
import authReducer from './reducer/authReducer';
import dataReducer from './reducer/dataReducer'

const rootReducer = combineReducers({
   auth: authReducer,
   data : dataReducer
})

export default rootReducer;
